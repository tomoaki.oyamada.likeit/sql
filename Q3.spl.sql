﻿create database practice DEFAULT CHARACTER SET utf8;
use practice;
create table item_category(
category_id int primary key not null auto_increment,
category_name VARCHAR(256) not null
);
insert into item_category values(1,'家具');
insert into item_category values(2,'食品');
insert into item_category values(3,'本');
