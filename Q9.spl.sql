﻿create database practice DEFAULT CHARACTER SET utf8;

use practice;

create table item(
ietm_id int primary key not null auto_increment,
item_name varchar(256) not null,
item_price int not null,
category_id int);

insert into item values(1,'堅牢な机',3000,1);
insert into item values(2,'生焼け肉',50,2);
insert into item values(3,'すっきりわかるJava入門',3000,3);
insert into item values(4,'おしゃれな椅子',2000,1);
insert into item values(5,'こんがり肉',500,2);
insert into item values(6,'書き方ドリルSQL',2500,3);
insert into item values(7,'ふわふわのベッド',30000,1);
insert into item values(8,'ミラノ風ドリア',300,2);

select item_name,item_price from item where category_id = 1;

select item_name,item_price from item where item_price>1000;

select item_name,item_price from item where item_name like '%肉%';
select ietm_id ,item_name,item_price,category_name from item  INNER JOIN
    item_category
ON
    item.category_id=item_category.category_id;
    

ALTER TABLE item RENAME COLUMN item_price TO total_price;

select category_name,total_price from item_category INNER JOIN
    item
ON
    item.category_id=item_category.category_id;
    

select * from item_category order by total_price desc;




