﻿create database practice DEFAULT CHARACTER SET utf8;

use practice;

create table item(
ietm_id int primary key not null auto_increment,
item_name varchar(256) not null,
item_price int not null,
category_id int);

